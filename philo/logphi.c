/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   logphi.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/23 05:18:01 by mlaneyri          #+#    #+#             */
/*   Updated: 2022/01/31 22:47:29 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./philosophers.h"

int	ft_itoa(char *s, int n)
{
	int	len;
	int	temp;

	temp = n;
	len = (n == 0);
	while (temp)
	{
		temp /= 10;
		len++;
	}
	temp = len;
	s[len] = ' ';
	while (len--)
	{
		s[len] = '0' + n % 10;
		n /= 10;
	}
	return (temp + 1);
}

void	print_event(t_table *table, int id, int event)
{
	static pthread_mutex_t	printlock = PTHREAD_MUTEX_INITIALIZER;
	static int				suppress_event = 0;
	static const char		*events[5] = {"is eating\n", "is sleeping\n",
		"is thinking\n", "has taken a fork\n", "died\n"};
	char					b[42];
	int						i;

	pthread_mutex_lock(&printlock);
	i = ft_itoa(b, get_ts(table));
	i += ft_itoa(b + i, id + 1);
	id = 0;
	while (events[event][id])
		b[i++] = events[event][id++];
	if (!suppress_event)
		write(1, b, i);
	if (event == EVENT_DIE)
		suppress_event = 1;
	pthread_mutex_unlock(&printlock);
}

int	philog(t_philo *phi, int event)
{
	int			ts;

	pthread_mutex_lock(&(phi->philock));
	if (!phi->death_notify)
	{
		ts = get_ts(phi->table);
		if (ts - phi->last_meal > phi->table->tt_die || event == EVENT_DIE)
		{
			print_event(phi->table, phi->id, EVENT_DIE);
			phi->death_notify = 1;
		}
		else
		{
			print_event(phi->table, phi->id, event);
			if (event == EVENT_EAT)
			{
				phi->meals_eaten++;
				phi->last_meal = ts;
			}
		}
	}
	phi->death_local = phi->death_notify;
	pthread_mutex_unlock(&(phi->philock));
	return (phi->death_local);
}
