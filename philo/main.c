/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/19 14:30:24 by mlaneyri          #+#    #+#             */
/*   Updated: 2022/02/03 18:53:20 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./philosophers.h"

pthread_t	*start_simulation(t_philo **philosophers, t_table *table)
{
	struct timeval	tv;
	int				i;
	pthread_t		*threads;

	threads = malloc(table->n * sizeof(pthread_t));
	if (!threads)
		return (NULL);
	i = -1;
	gettimeofday(&tv, NULL);
	philosophers[0]->table->start_date = tv;
	while (++i < table->n)
		pthread_create(&(threads[i]), NULL, &simulation, philosophers[i]);
	return (threads);
}

int	kill_all(t_philo **phi)
{
	int	i;

	i = -1;
	while (++i < phi[0]->table->n)
	{
		pthread_mutex_lock(&(phi[i]->philock));
		phi[i]->death_notify = 1;
		pthread_mutex_unlock(&(phi[i]->philock));
	}
	return (0);
}

void	check_phi(t_philo *phi, t_table *table)
{
	int	ts;

	pthread_mutex_lock(&(phi->philock));
	ts = get_ts(table);
	if (phi->death_notify || ts - phi->last_meal > table->tt_die)
	{
		if (!phi->death_notify)
			print_event(table, phi->id, EVENT_DIE);
		phi->death_notify = 1;
		table->death = 1;
	}
	if (phi->meals_eaten < table->to_eat)
		table->full = 0;
	pthread_mutex_unlock(&(phi->philock));
}

int	end_simulation(t_philo **phi, t_table *table)
{
	int	i;

	while (!table->death)
	{
		usleep(500);
		table->full = 1;
		i = -1;
		while (++i < table->n)
			check_phi(phi[i], table);
		if (table->death || (table->to_eat > 0 && table->full))
			kill_all(phi);
	}
	return (0);
}

int	main(int ac, char **av)
{
	t_philo		**phi;
	t_table		*table;
	pthread_t	*threads;
	int			i;

	if (ac < 5 || ac > 6)
		return (-1);
	phi = create_all(ac, av);
	if (!phi)
		return (-2);
	table = phi[0]->table;
	threads = start_simulation(phi, table);
	if (!threads)
		return ((long)destroy_all(phi, -1) - 3);
	end_simulation(phi, table);
	i = -1;
	while (++i < table->n)
		pthread_join(threads[i], NULL);
	free(threads);
	destroy_all(phi, -1);
	return (0);
}
