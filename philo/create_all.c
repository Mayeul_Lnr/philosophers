/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_all.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/22 11:04:03 by mlaneyri          #+#    #+#             */
/*   Updated: 2022/02/05 23:28:55 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./philosophers.h"

int	create_mutex(t_table *table)
{
	int	i;

	table->forks = malloc(table->n * sizeof(pthread_mutex_t));
	if (!table->forks)
		return (-1);
	i = -1;
	while (++i < table->n)
		pthread_mutex_init(&(table->forks[i]), NULL);
	return (0);
}

t_table	*create_table(int ac, char **av)
{
	t_table	*ret;

	ret = malloc(sizeof(t_table));
	if (!ret)
		return (NULL);
	ret->n = ft_atoi(av[1]);
	if (ac == 6)
		ret->to_eat = ft_atoi(av[5]);
	else
		ret->to_eat = -1;
	if (ret->n < 1 || (ac == 6 && ret->to_eat < 1))
		return (ft_free(ret));
	ret->tt_die = ft_atoi(av[2]);
	ret->tt_eat = ft_atoi(av[3]);
	ret->tt_sleep = ft_atoi(av[4]);
	if (ret->tt_die < 0 || ret->tt_eat < 0 || ret->tt_sleep < 0)
		return (ft_free(ret));
	ret->full = 0;
	ret->death = 0;
	if (create_mutex(ret) < 0)
		return (ft_free(ret));
	return (ret);
}

t_philo	*create_philo(int id, t_table *table)
{
	t_philo	*ret;

	ret = malloc(sizeof(t_philo));
	if (!ret)
		return (NULL);
	memset(ret, 0, sizeof(t_philo));
	pthread_mutex_init(&(ret->philock), NULL);
	ret->id = id;
	ret->table = table;
	return (ret);
}

t_philo	**create_all(int ac, char **av)
{
	t_table	*table;
	t_philo	**ret;
	int		i;

	table = create_table(ac, av);
	if (!table)
		return (NULL);
	ret = malloc(table->n * sizeof(t_philo));
	if (!ret)
		return ((t_philo **)destroy_table(table));
	i = -1;
	while (++i < table->n)
	{
		ret[i] = create_philo(i, table);
		if (!ret[i])
		{
			while (i >= 0)
				destroy_philo(ret[i--]);
			return ((t_philo **)destroy_table(table));
		}
	}
	return (ret);
}
