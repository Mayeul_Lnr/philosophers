/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   life.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/23 08:35:08 by mlaneyri          #+#    #+#             */
/*   Updated: 2022/02/03 19:38:04 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./philosophers.h"

int	grab_fork(t_philo *phi)
{
	if (phi->id % 2)
	{
		pthread_mutex_lock(&(phi->table->forks[(phi->id + phi->table->n - 1)
				% phi->table->n]));
		philog(phi, EVENT_FORK);
		pthread_mutex_lock(&(phi->table->forks[phi->id]));
	}
	else
	{
		pthread_mutex_lock(&(phi->table->forks[phi->id]));
		philog(phi, EVENT_FORK);
		if (phi->table->n == 1)
			return (usleep(1000 * phi->table->tt_die) + philog(phi, EVENT_DIE));
		pthread_mutex_lock(&(phi->table->forks[(phi->id + phi->table->n - 1)
				% phi->table->n]));
	}
	return (philog(phi, EVENT_FORK));
}

int	eat(t_philo *phi)
{
	int	ret;

	if (!grab_fork(phi) && !philog(phi, EVENT_EAT))
	{
		ret = 1000 * (phi->table->tt_eat + phi->last_meal)
			- get_uts(phi->table);
		if (ret > 0)
			usleep(ret);
	}
	phi->last_sleep = get_ts(phi->table);
	ret = philog(phi, EVENT_SLEEP);
	pthread_mutex_unlock(&(phi->table->forks[phi->id]));
	if (phi->table->n != 1)
		pthread_mutex_unlock(&(phi->table->forks[(phi->id + phi->table->n - 1)
				% phi->table->n]));
	return (ret);
}

void	*simulation(void *philosopher)
{
	t_philo	*phi;
	t_table	*table;
	int		dodo;

	phi = (t_philo *)philosopher;
	table = phi->table;
	if (phi->id % 2)
		usleep(500 * table->tt_eat);
	while (1)
	{
		if (eat(phi))
			break ;
		dodo = 1000 * (table->tt_sleep + phi->last_sleep)
			- get_uts(table);
		if (dodo > 0)
			usleep(dodo);
		if (philog(phi, EVENT_THINK))
			break ;
		if (phi->table->tt_die - get_ts(table) + phi->last_meal
			> (table->tt_eat + table->tt_sleep) / 2)
			usleep(500 * (table->tt_eat + table->tt_sleep));
	}
	return (NULL);
}
