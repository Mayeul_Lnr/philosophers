/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   destroy_all.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/22 11:04:03 by mlaneyri          #+#    #+#             */
/*   Updated: 2022/01/31 18:09:16 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./philosophers.h"

t_table	*destroy_table(t_table *table)
{
	int	i;

	i = -1;
	while (++i < table->n)
		pthread_mutex_destroy(&(table->forks[i]));
	free(table->forks);
	return (ft_free(table));
}

t_philo	*destroy_philo(t_philo *philo)
{
	pthread_mutex_destroy(&(philo->philock));
	free(philo);
	return (NULL);
}

t_philo	**destroy_all(t_philo **philosophers, int n)
{
	int		i;
	t_table	*table;

	if (!n)
		return (NULL);
	if (n < 0)
		n = philosophers[0]->table->n;
	table = philosophers[0]->table;
	i = -1;
	while (++i < n)
		destroy_philo(philosophers[i]);
	free(philosophers);
	destroy_table(table);
	return (NULL);
}
