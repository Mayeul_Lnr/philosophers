/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   timestamp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/23 05:06:07 by mlaneyri          #+#    #+#             */
/*   Updated: 2022/01/27 02:24:03 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./philosophers.h"

t_ull	get_uts(t_table *table)
{
	struct timeval	tv;

	gettimeofday(&tv, NULL);
	return (1000000 * (tv.tv_sec - table->start_date.tv_sec)
		+ tv.tv_usec - table->start_date.tv_usec);
}

t_ull	get_ts(t_table *table)
{
	struct timeval	tv;

	gettimeofday(&tv, NULL);
	return (1000 * (tv.tv_sec - table->start_date.tv_sec)
		+ (tv.tv_usec - table->start_date.tv_usec) / 1000);
}
