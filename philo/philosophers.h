/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/19 14:31:04 by mlaneyri          #+#    #+#             */
/*   Updated: 2022/02/13 18:25:18 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILOSOPHERS_H
# define PHILOSOPHERS_H

# include <stdlib.h>
# include <stdio.h>
# include <unistd.h>
# include <sys/time.h>
# include <pthread.h>
# include <string.h>

# define EVENT_EAT 0
# define EVENT_SLEEP 1
# define EVENT_THINK 2
# define EVENT_FORK 3
# define EVENT_DIE 4
# define EVENT_FIVE 5

# define SLEEP_STEP 100

typedef int	t_ull;

typedef struct s_table
{
	pthread_mutex_t	*forks;
	struct timeval	start_date;
	int				n;
	int				full;
	int				death;
	int				tt_die;
	int				tt_eat;
	int				tt_sleep;
	int				to_eat;
}				t_table;

typedef struct s_philo
{
	pthread_mutex_t	philock;
	int				death_notify;
	int				death_local;
	int				id;
	int				last_meal;
	int				last_sleep;
	int				meals_eaten;
	t_table			*table;
}				t_philo;

/*
**	Constructors in create_all.c :
*/

t_table	*create_table(int ac, char **av);

t_philo	*create_philo(int id, t_table *table);

t_philo	**create_all(int ac, char **av);

/*
**	Destructors in destroy_all.c :
*/

t_table	*destroy_table(t_table *table);

t_philo	*destroy_philo(t_philo *philo);

t_philo	**destroy_all(t_philo **philosophers, int n);

/*
**	Utilitaries in ft_utils.c
*/

int		ft_atoi(const char *str);

void	*ft_free(void *ptr);

/*
**	Others :
*/

int		philog(t_philo *phi, int event);

void	print_event(t_table *table, int id, int event);

void	*simulation(void *philosopher);

/*
**	Time manipulation functions in timestamp.c
*/

t_ull	get_ts(t_table *table);

t_ull	get_uts(t_table *table);

#endif
