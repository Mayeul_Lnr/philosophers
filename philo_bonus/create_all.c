/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   create_all.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/22 11:04:03 by mlaneyri          #+#    #+#             */
/*   Updated: 2022/02/03 19:43:14 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./philosophers.h"

int	create_table(int ac, char **av, t_phidata *phi)
{
	phi->n = ft_atoi(av[1]);
	if (ac == 6)
		phi->to_eat = ft_atoi(av[5]);
	else
		phi->to_eat = -1;
	if (phi->n < 1 || (ac == 6 && phi->to_eat < 1))
		return (-1);
	phi->tt_die = ft_atoi(av[2]);
	phi->tt_eat = ft_atoi(av[3]);
	phi->tt_sleep = ft_atoi(av[4]);
	if (phi->tt_die < 0 || phi->tt_eat < 0 || phi->tt_sleep < 0)
		return (-1);
	return (0);
}

char	*sem_name(char *s, int n)
{
	if (n < 0 || n > 999)
		return (NULL);
	ft_itoa(s + 8, n);
	return (s);
}

int	data_protecc_init(t_phidata *phi)
{
	int			i;
	static char	s1[] = "/phidata   ";

	phi->data_protecc = malloc((phi->n + 1) * sizeof(sem_t *));
	if (!phi->data_protecc)
		return (close_all(phi) - 1);
	i = -1;
	while (++i <= phi->n)
	{
		sem_name(s1, i);
		sem_unlink(s1);
		phi->data_protecc[i] = sem_open(s1, O_CREAT, 0600, 1);
		if (!phi->data_protecc[i])
			return (close_all(phi) - 1);
	}
	return (0);
}

int	main_init(int ac, char **av, t_phidata *phi)
{
	memset(phi, 0, sizeof(t_phidata));
	if (create_table(ac, av, phi))
		return (-1);
	sem_unlink("/phidead");
	sem_unlink("/phifull");
	sem_unlink("/phiends");
	sem_unlink("/phifork");
	sem_unlink("/phiprnt");
	phi->im_dead = sem_open("/phidead", O_CREAT, 0600, phi->n);
	phi->im_full = sem_open("/phifull", O_CREAT, 0600, phi->n);
	phi->end_sim = sem_open("/phiends", O_CREAT, 0600, phi->n);
	phi->forks = sem_open("/phifork", O_CREAT, 0600, phi->n + 1);
	phi->print_protecc = sem_open("/phiprnt", O_CREAT, 0666, 1);
	if (phi->im_dead == SEM_FAILED || phi->im_full == SEM_FAILED
		|| phi->end_sim == SEM_FAILED || phi->forks == SEM_FAILED
		|| phi->print_protecc == SEM_FAILED)
		return (close_all(phi) - 1);
	return (data_protecc_init(phi));
}
