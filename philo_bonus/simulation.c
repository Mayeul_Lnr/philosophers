/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   simulation.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/23 08:35:08 by mlaneyri          #+#    #+#             */
/*   Updated: 2022/02/03 19:45:36 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./philosophers.h"

int	eat(t_phidata *phi)
{
	int	ret;

	sem_wait(phi->forks);
	philog(phi, EVENT_FORK);
	if (phi->n == 1)
		return (usleep(1000 * phi->tt_die) + philog(phi, EVENT_DIE));
	sem_wait(phi->forks);
	philog(phi, EVENT_FORK);
	if (!philog(phi, EVENT_EAT))
	{
		ret = 1000 * (phi->tt_eat + phi->last_meal)
			- get_uts(phi);
		//printf("MIAM %d\n", ret);
		if (ret > 0)
			usleep(ret);
	}
	phi->last_sleep = get_ts(phi);
	ret = philog(phi, EVENT_SLEEP);
	sem_post(phi->forks);
	sem_post(phi->forks);
	return (ret);
}

void	simulation(t_phidata *phi)
{
	int	dodo;

	while (1)
	{
		if (eat(phi))
			break ;
		dodo = 1000 * (phi->tt_sleep + phi->last_sleep) - get_uts(phi);
		if (dodo > 0)
			usleep(dodo);
		if (philog(phi, EVENT_THINK))
			break ;
		if (phi->tt_die - get_ts(phi) + phi->last_meal
			> (phi->tt_eat + phi->tt_sleep) / 2)
			usleep(420 * (phi->tt_eat + phi->tt_sleep));
	}
}

int	start_simulation(t_phidata *phi)
{
	pthread_t	threads[2];

	sem_wait(phi->im_full);
	sem_wait(phi->im_dead);
	pthread_create(&(threads[0]), NULL, &monitor, phi);
	pthread_create(&(threads[1]), NULL, &wait_endsim, phi);
	if (phi->id % 2)
		usleep(500 * phi->tt_eat);
	simulation(phi);
	if (phi->n == 1)
		sem_post(phi->forks);
	pthread_join(threads[0], NULL);
	pthread_join(threads[1], NULL);
	if (phi->death_notify)
		return (42);
	return (0);
}
