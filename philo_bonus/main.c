/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/19 14:30:24 by mlaneyri          #+#    #+#             */
/*   Updated: 2022/02/03 19:45:08 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./philosophers.h"

int	*create_forks(t_phidata *phi)
{
	int		*forks;
	int		i;

	forks = malloc(phi->n * sizeof(int));
	if (!forks)
		return (NULL);
	i = -1;
	while (++i < phi->n)
		sem_wait(phi->end_sim);
	sem_wait(phi->forks);
	return (forks);
}

int	*launch_forks(t_phidata *phi)
{
	t_tv	tv;
	int		i;
	int		*forks;

	forks = create_forks(phi);
	if (!forks)
		return (NULL);
	i = -1;
	gettimeofday(&tv, NULL);
	phi->start_date = tv;
	while (++i < phi->n)
	{
		forks[i] = fork();
		if (!forks[i])
			break ;
		if (forks[i] < 0)
			return ((int *)ft_free(forks));
	}
	phi->id = i;
	if (i != phi->n)
	{
		phi->endsim = start_simulation(phi);
		return (ft_free(forks));
	}
	return (forks);
}

void	main_monitor_start(t_phidata *phi)
{
	pthread_t	threads[2];

	pthread_create(&(threads[0]), NULL, &wait_death, phi);
	pthread_create(&(threads[1]), NULL, &wait_full, phi);
	pthread_join(threads[0], NULL);
	pthread_join(threads[1], NULL);
	phi->id = -1;
}

int	main(int ac, char **av)
{
	t_phidata	phi;
	int			*forks;

	if (ac < 5 || ac > 6)
		return (-1);
	if (main_init(ac, av, &phi))
		return (-2);
	forks = launch_forks(&phi);
	if (forks)
	{
		main_monitor_start(&phi);
		phi.full = phi.n + 0 * sem_post(phi.forks);
		while (--phi.full > -1)
		{
			waitpid(forks[phi.full], &(phi.endsim), 0);
			if (WIFEXITED(phi.endsim) && WEXITSTATUS(phi.endsim) == 42)
				phi.id = phi.full;
		}
		if (phi.id > -1)
			printf("%d %d died\n", phi.last_meal, phi.id + 1);
		free(forks);
		return (end_parent(&phi));
	}
	else
		return (phi.endsim + end_child(&phi));
}
