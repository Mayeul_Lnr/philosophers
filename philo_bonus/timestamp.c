/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   timestamp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/23 05:06:07 by mlaneyri          #+#    #+#             */
/*   Updated: 2022/02/01 18:04:09 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./philosophers.h"

t_ull	get_uts(t_phidata *phi)
{
	t_tv	tv;

	gettimeofday(&tv, NULL);
	return (1000000 * (tv.tv_sec - phi->start_date.tv_sec)
		+ tv.tv_usec - phi->start_date.tv_usec);
}

t_ull	get_ts(t_phidata *phi)
{
	t_tv	tv;

	gettimeofday(&tv, NULL);
	return (1000 * (tv.tv_sec - phi->start_date.tv_sec)
		+ (tv.tv_usec - phi->start_date.tv_usec) / 1000);
}
