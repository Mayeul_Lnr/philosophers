/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   monitor.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/19 14:30:24 by mlaneyri          #+#    #+#             */
/*   Updated: 2022/02/03 19:44:01 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./philosophers.h"

void	*wait_death(void *ptr)
{
	t_phidata	*phi;
	int			i;

	phi = (t_phidata *)ptr;
	usleep(1000 * phi->tt_die);
	sem_wait(phi->im_dead);
	sem_post(phi->im_dead);
	phi->last_meal = get_ts(phi);
	sem_wait(phi->data_protecc[phi->id]);
	if (!phi->death_notify)
	{
		phi->death_notify = 1;
		i = -1;
		while (++i < phi->n)
			sem_post(phi->end_sim);
	}
	sem_post(phi->data_protecc[phi->id]);
	return (NULL);
}

void	*wait_full(void *ptr)
{
	t_phidata	*phi;
	int			i;

	phi = (t_phidata *)ptr;
	usleep(1000 * phi->tt_eat);
	i = -1;
	while (++i < phi->n)
		sem_wait(phi->im_full);
	sem_wait(phi->data_protecc[phi->id]);
	if (!phi->death_notify)
	{
		phi->death_notify = 1;
		i = -1;
		while (++i < phi->n)
			sem_post(phi->end_sim);
		sem_post(phi->forks);
	}
	sem_post(phi->data_protecc[phi->id]);
	i = -1;
	while (++i < phi->n)
		sem_post(phi->im_full);
	return (NULL);
}

void	*wait_endsim(void *ptr)
{
	t_phidata	*phi;

	phi = (t_phidata *)ptr;
	sem_wait(phi->end_sim);
	sem_post(phi->end_sim);
	sem_wait(phi->data_protecc[phi->id]);
	if (!phi->death_notify)
		sem_post(phi->im_dead);
	phi->endsim = 1;
	if (!phi->full)
		sem_post(phi->im_full);
	sem_post(phi->data_protecc[phi->id]);
	return (NULL);
}

void	*monitor(void *ptr)
{
	t_phidata	*phi;
	int			ts;

	phi = (t_phidata *)ptr;
	ts = 1;
	while (ts >= 0)
	{
		usleep(500);
		sem_wait(phi->data_protecc[phi->id]);
		ts = get_ts(phi);
		if (phi->death_notify || ts - phi->last_meal > phi->tt_die)
		{
			if (!phi->death_notify)
				sem_post(phi->im_dead);
			if (!phi->full)
				sem_post(phi->im_full);
			phi->death_notify = 1;
		}
		if (phi->endsim || phi->death_notify)
			ts = -1;
		sem_post(phi->data_protecc[phi->id]);
	}
	return (NULL);
}
