/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   logphi.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/23 05:18:01 by mlaneyri          #+#    #+#             */
/*   Updated: 2022/02/02 19:26:59 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./philosophers.h"

int	ft_itoa(char *s, int n)
{
	int	len;
	int	temp;

	temp = n;
	len = (n == 0);
	while (temp)
	{
		temp /= 10;
		len++;
	}
	temp = len;
	s[len] = ' ';
	while (len--)
	{
		s[len] = '0' + n % 10;
		n /= 10;
	}
	return (temp + 1);
}

void	print_event(t_phidata *phi, int event)
{
	static const char	*events[5] = {"is eating\n", "is sleeping\n",
		"is thinking\n", "has taken a fork\n", "died\n"};
	char				b[42];
	int					i;
	int					j;

	sem_wait(phi->print_protecc);
	i = ft_itoa(b, get_ts(phi));
	i += ft_itoa(b + i, phi->id + 1);
	j = 0;
	while (events[event][j])
		b[i++] = events[event][j++];
	if (!phi->print_suppress)
		write(1, b, i);
	if (event == EVENT_DIE)
		phi->print_suppress = 1;
	sem_post(phi->print_protecc);
}

void	classic_log(t_phidata *phi, int event, int ts)
{
	print_event(phi, event);
	if (event == EVENT_EAT)
	{
		phi->meals_eaten++;
		phi->last_meal = ts;
		if (phi->meals_eaten == phi->to_eat)
		{
			phi->full = 1;
			sem_post(phi->im_full);
		}
	}
}

int	philog(t_phidata *phi, int event)
{
	int	ts;
	int	death_local;

	sem_wait(phi->data_protecc[phi->id]);
	if (!phi->death_notify && !phi->endsim)
	{
		ts = get_ts(phi);
		if (ts - phi->last_meal > phi->tt_die || event == EVENT_DIE)
		{
			sem_post(phi->im_dead);
			if (!phi->full)
				sem_post(phi->im_full);
			phi->death_notify = 1;
		}
		else
			classic_log(phi, event, ts);
	}
	death_local = phi->death_notify + phi->endsim;
	sem_post(phi->data_protecc[phi->id]);
	return (death_local);
}
