/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   end_all.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/01 22:02:49 by mlaneyri          #+#    #+#             */
/*   Updated: 2022/02/02 20:18:36 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./philosophers.h"

int	unlink_all(t_phidata *phi)
{
	static char	s[] = "/phidata   ";
	int			i;

	if (phi->im_dead)
		sem_unlink("/phidead");
	if (phi->im_full)
		sem_unlink("/phifull");
	if (phi->end_sim)
		sem_unlink("/phiends");
	if (phi->forks)
		sem_unlink("/phifork");
	if (phi->print_protecc)
		sem_unlink("/phiprnt");
	if (phi->data_protecc)
	{
		i = -1;
		while (++i <= phi->n)
			if (phi->data_protecc[i])
				sem_unlink(sem_name(s, i));
	}
	return (0);
}

int	close_all(t_phidata *phi)
{
	int	i;

	if (phi->im_dead)
		sem_close(phi->im_dead);
	if (phi->im_full)
		sem_close(phi->im_full);
	if (phi->end_sim)
		sem_close(phi->end_sim);
	if (phi->forks)
		sem_close(phi->forks);
	if (phi->print_protecc)
		sem_close(phi->print_protecc);
	if (phi->data_protecc)
	{
		i = -1;
		while (++i <= phi->n)
			if (phi->data_protecc[i])
				sem_close(phi->data_protecc[i]);
	}
	return (0);
}

int	end_parent(t_phidata *phi)
{
	close_all(phi);
	unlink_all(phi);
	free(phi->data_protecc);
	return (0);
}

int	end_child(t_phidata *phi)
{
	close_all(phi);
	free(phi->data_protecc);
	return (0);
}
