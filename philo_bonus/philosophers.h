/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   philosophers.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlaneyri <mlaneyri@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/19 14:31:04 by mlaneyri          #+#    #+#             */
/*   Updated: 2022/02/02 18:16:01 by mlaneyri         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PHILOSOPHERS_H
# define PHILOSOPHERS_H

# include <stdlib.h>
# include <stdio.h>
# include <unistd.h>
# include <sys/time.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <semaphore.h>
# include <pthread.h>
# include <sys/types.h>
# include <sys/wait.h>
# include <string.h>

# define EVENT_EAT 0
# define EVENT_SLEEP 1
# define EVENT_THINK 2
# define EVENT_FORK 3
# define EVENT_DIE 4

typedef int				t_ull;

typedef struct timeval	t_tv;

typedef struct s_phidata
{
	sem_t	*im_dead;
	sem_t	*im_full;
	sem_t	*end_sim;
	sem_t	*forks;
	sem_t	*print_protecc;
	sem_t	**data_protecc;
	t_tv	start_date;
	int		n;
	int		tt_die;
	int		tt_eat;
	int		tt_sleep;
	int		to_eat;
	int		id;
	int		last_meal;
	int		last_sleep;
	int		meals_eaten;
	int		death_notify;
	int		endsim;
	int		print_suppress;
	int		full;
}				t_phidata;

/*
**	Constructors and destructors in create_all.c and end_all.c :
*/

int		create_table(int ac, char **av, t_phidata *phi);

int		main_init(int ac, char **av, t_phidata *phi);

int		close_all(t_phidata *phi);

char	*sem_name(char *s, int n);

int		end_parent(t_phidata *phi);

int		end_child(t_phidata *phi);

/*
**	Utilitaries in ft_utils.c
*/

int		ft_atoi(const char *str);

void	*ft_free(void *ptr);

/*
**	Others :
*/

int		ft_itoa(char *s, int n);

int		philog(t_phidata *phi, int event);

void	print_event(t_phidata *phi, int event);

int		start_simulation(t_phidata *phi);

/*
**	Time manipulation functions in timestamp.c
*/

t_ull	get_ts(t_phidata *phi);

t_ull	get_uts(t_phidata *phi);

/*
**	Monitoring functions in monitor.c
*/

void	*wait_death(void *ptr);

void	*wait_full(void *ptr);

void	*monitor(void *ptr);

void	*wait_endsim(void *ptr);

#endif
